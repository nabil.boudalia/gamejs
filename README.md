# Jeu en JavaScript - Simplon

Réaliser un jeu en javascript

## Les jeux de stratégie - Définition

Egalement appellé jeu de stratégie combinatoire abstrait ou jeu combinatoire à information parfaite selon la théorie des jeux combinatoires. Les critères pour être catégorisés sont : 

*   Oppose 2 joueurs ou 2 équipes contre un adversaire qui peut être un ordinateur
*   Jouent à tour de rôle 
*   Les éléments du jeux sont connues de tout les parties prenant 
*   Il a y peu de place au hasard

Voici une liste d'autre jeu qui repondent à se critères : jeux d'échec, dames, tablut, jeu de go, xiangqi, shōgi, bagh chal, congkak, Fanorona ...

## Pourquoi ce choix ?

Car la stratégie est le carrefour de plusieurs discipline.

La thérie des jeux, concept majeur en économie, étudie mathématiquement les interactions des choix individuel.

En mathématique, on retrouve se concept à travers la théorie des jeux, et dans les jeux de stratégie combinatoire abstrait, notamment à travers les nombres suréels. 

L'informatique est le croissement parfait entre toute ces composantes. Alphago en est le témoin parfait qui croisent le domaine informatique, mathématique et algorithme.

[![img](img/alpha.svg)]()

## Maquette

[![img](img/GameJS.png)]()



